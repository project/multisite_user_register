<?php

namespace Drupal\multisite_user_register\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Multi Site User Register entities.
 */
interface MultiSiteUserRegisterInterface extends ConfigEntityInterface {

  // Add get/set methods for your configuration properties here.
}